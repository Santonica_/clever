<footer class="footer-wrapper">
    <div class="footer-container container">

        <?php
            if ( is_active_sidebar('footer-sidebar') ){
                dynamic_sidebar('footer-sidebar');
            }
        ?>

    </div>

    <div class="copyright-wrapper">
        <div class="copyright-container container clearfix">
            <div class="copyright-left">Theme By Santonica</div>
            <div class="copyright-right">
                Copyright © 2017 - All Right Reserved - Santonica
            </div>
        </div>
    </div>

</footer>

<?php wp_footer(); ?>

    </body>
</html>