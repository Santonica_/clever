jQuery(document).ready(function($) {
    $('.flexslider').flexslider({
        animation: "slide",
        slideshowSpeed: 3000,
        reverse: false,
        animationLoop: true,
        controlNav: true,
        directionNav: false,
        itemWidth: 210,
        itemMargin: 20,
        minItems: 2,
        maxItems: 3
    });
    $('.flexslider-t').flexslider({
        animation: "slide",
        slideshowSpeed: 3000,
        reverse: false,
        animationLoop: true,
        controlNav: false,
        directionNav: true,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 1,
        maxItems: 1
    });
    $('.flexslider-tw').flexslider({
        animation: "slide",
        slideshowSpeed: 4000,
        reverse: false,
        animationLoop: true,
        controlNav: false,
        directionNav: true,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 1,
        maxItems: 1
    });
});