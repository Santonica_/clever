<?php get_header(); ?>

<?php
if ( have_posts() ) :

        get_template_part( 'loop', 'post' );

else : ?>

    <p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'twentyseventeen' ); ?></p>
    <?php

endif;
?>

<?php get_footer(); ?>