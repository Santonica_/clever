<!DOCTYPE>
<html>
  <head>
        <title><?php wp_title(''); ?> <?php bloginfo( 'name' ); ?></title>
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
    <header id="header">
        <nav class="navbar navbar-default navbar-fixed-top">

            <?php
            if ( is_active_sidebar('header-sidebar') ){
                dynamic_sidebar('header-sidebar');
            }
            ?>

            <div class="container main-nav-container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo home_url(); ?>">
                        <?php if( $logo = get_custom_logo() ){
                            echo $logo;
                        } else { ?>
                        <img src="<?php echo get_template_directory_uri() ?>/img/logo.png" alt="logo">
                        <?php } ?>
                    </a>
                </div>

                <div class="navbar-search navbar-right">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </div>
                <?php wp_nav_menu( array(
                    'theme_location'  => 'primary',
                    'menu'  => 'header menu',
                    'container'  => 'div',
                    'container_class'  => 'collapse navbar-collapse',
                    'container_id'  => 'bs-example-navbar-collapse-1',
                    'menu_class'  => 'nav navbar-nav navbar-right',
                ));
                ?>

            </div>
        </nav>
    </header>