<?php get_header(); ?>

<?php if ( have_posts() ) : ?>

    <div class="page-title-wrapper">
        <div class="page-title-overlay"></div>
        <div class="page-title-container container">
            <?php
            the_archive_title( '<h1 class="page-title">', '</h1>' );
            the_archive_description( '<span class="page-caption">', '</span>' );
            ?>
        </div>
    </div>

    <div class="content-wrapper">
        <div class="archive-container container">
            <div class="row">

            <?php while ( have_posts() ) : the_post(); ?>

                <div class="archive-item col-md-4">
                    <?php if ( has_post_thumbnail()) { ?>
                        <div class="archive-item-thumbnail">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail(array(400, 300), array('class' => 'img-responsive')); ?>
                            </a>
                        </div>
                    <?php } ?>

                    <h3 class="archive-item-title">
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </h3>
                    <div class="archive-item-info">
                        <div class="archive-item-date">
                            <span>Posted on</span>
                            <a href="#"><?php the_time('j F Y'); ?></a>
                        </div>
                        <div class="archive-item-tag">
                            <span>Tags</span>
                            <a href="#"><?php the_tags('', ', '); ?></a>
                        </div>
                        <div class="archive-item-comment">
                            <span>Comments</span>
                            <a href="#">0</a>
                        </div>
                    </div>
                    <div class="archive-item-content">
                        <?php the_excerpt(); ?>
                    </div>
                    <a href="<?php the_permalink(); ?>" class="button-read-more">Read more</a>
                </div>

            <?php endwhile; ?>

            </div>
        </div>
    </div>
    <?php
    the_posts_pagination( array(
        'prev_text'          => __( 'Previous page', 'sg' ),
        'next_text'          => __( 'Next page', 'sg' ),
    ) );
    ?>

<?php endif; ?>

<?php get_footer(); ?>