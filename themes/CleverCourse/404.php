<?php get_header(); ?>

    <div class="page-title-wrapper">
        <div class="page-title-overlay"></div>
        <div class="page-title-container container">
            <h1 class="page-title">404</h1>
            <span class="page-caption">Page not found</span>
        </div>
    </div>

    <div class="content-wrapper">
        <div class="page-not-found-container container">
            <div class="page-not-found-block">
                <div class="page-not-found-icon">
                    <span class="glyphicon glyphicon-hand-down"></span>
                </div>
                <div class="page-not-found-title">
                    <?php _e('Error 404', 'sg'); ?>
                </div>
                <div class="page-not-found-caption">
                    <?php _e('Sorry, we couldn\'t find the page you\'re looking for.', 'gdlr_translate'); ?>
                </div>
                <div class="page-not-found-search">
                    <?php get_search_form(); ?>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>