<?php get_header(); ?>

<?php if ( have_posts() ) : ?>

    <div class="page-title-wrapper">
        <div class="page-title-overlay"></div>
        <div class="page-title-container container">
            <?php
            the_archive_title( '<h1 class="page-title">', '</h1>' );
            the_archive_description( '<span class="page-caption">', '</span>' );
            ?>
        </div>
    </div>

    <div class="content-wrapper">
        <div class="instructor-container container">
            <div class="instructor-wrapper row">

                <?php while ( have_posts() ) : the_post(); ?>

                <div class="col-md-4 no-padding">
                    <div class="instructor-item-wrapper">
                        <div class="instructor-content">
                            <?php if ( has_post_thumbnail()) { ?>
                                <div class="instructor-thumbnail">
                                    <?php the_post_thumbnail('instructor-thumb'); ?>
                                </div>
                            <?php } ?>
                            <div class="instructor-title-wrapper">
                                <h3 class="instructor-title"><?php the_title(); ?></h3>
                                <div class="instructor-position">
                                    <?php echo get_post_meta(get_the_ID(), 'instructor_position', true); ?>
                                </div>
                            </div>
                            <div class="instructor-description">
                                <?php the_excerpt(); ?>
                            </div>
                            <a class="instructor-button" href="<?php the_permalink(); ?>">View profile</a>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>

            </div>
        </div>
    </div>
    <?php
    the_posts_pagination( array(
        'prev_text'          => __( 'Previous page', 'sg' ),
        'next_text'          => __( 'Next page', 'sg' ),
    ) );
    ?>

<?php endif; ?>

<?php get_footer(); ?>