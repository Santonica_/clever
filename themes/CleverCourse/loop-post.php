<?php if ( have_posts() ) : ?>
        <div class="page-title-wrapper">
            <div class="page-title-overlay"></div>
            <div class="page-title-container container">
                <h1 class="page-title"><?php the_title(); ?></h1>
            </div>
        </div>
    <?php while ( have_posts() ) : the_post(); ?>
        <div class="content-wrapper">
            <div class="container">
                <div class="post-single">
                    <a href="<?php the_permalink() ?>">
                        <h1 class="post-single-title"><?php the_title(); ?></h1>
                    </a>
                    <h2>
                        <span class="single-post-date">
                            <span class="glyphicon glyphicon-time"></span>
                            <?php echo __('Posted on', 'sg'); ?> <?php the_time('F j, Y @ g:i'); ?>
                        </span>
                    </h2>
                    <div class="post-image">
                        <?php if ( has_post_thumbnail() ){
                            the_post_thumbnail(array(900, 300), array('class' => 'img-responsive'));
                        } ?>
                    </div>
                    <div class="post-content">
                        <p><?php the_content(); ?></p>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>