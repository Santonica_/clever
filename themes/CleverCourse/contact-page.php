<?php /* Template Name: contact-page */ ?>

<?php get_header(); ?>

<?php

$location = get_field('location');

if( !empty($location) ):
    ?>
    <div class="acf-map">
        <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
    </div>
<?php endif; ?>

    <div class="container container-contact">

        <div class="row">

            <div class="col-md-8">

                <?php
                if ( is_active_sidebar('contact-form-sidebar') ){
                    dynamic_sidebar('contact-form-sidebar');
                }
                ?>

            </div>

            <div class="col-md-4">

                <?php
                if ( is_active_sidebar('contact-right-sidebar') ){
                    dynamic_sidebar('contact-right-sidebar');
                }
                ?>

            </div>
        </div>
    </div>


<?php get_footer(); ?>