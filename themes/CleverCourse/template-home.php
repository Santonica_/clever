<?php /* Template Name: Home */ ?>
<?php get_header(); ?>

    <div class="content-wrapper">

        <?php
            global $post;
            $postID = $post->ID;
            $slides_count = get_post_meta($postID, 'slider', true);
            if ( !empty($slides_count) ){
        ?>
        <div id="carousel-example-generic" class="carousel slide slider" data-interval="3000" data-ride="carousel">
            <ol class="carousel-indicators">
                <?php for ( $i = 0; $i < $slides_count; $i++){ ?>
                    <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i?>" <?php  if ($i == 0) echo 'class="active"' ?>></li>
                <?php } ?>
            </ol>
            <div class="carousel-inner" role="listbox">
                 <?php
                    for ( $i = 0; $i < $slides_count; $i++){
                        $imgID = get_post_meta($postID, 'slider_' . $i . '_main', true);
                        $slide = wp_get_attachment_image_url($imgID, 'full');
                    ?>
                        <div class="item <?php  if ($i == 0) echo "active" ?>">
                            <img src="<?php echo $slide ?>" alt="">
                            <div class="carousel-caption">

                            </div>
                        </div>
                    <?php } ?>
            </div>
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
             </div>
        <?php } ?>

        <?php
            $quickers = array();
            $quickers = get_post_meta($postID, 'q_info', true);
            if ( !empty($quickers) ){
                $the_query = new WP_Query( array(
                        'post_type' => 'quick_info',
                        'p'         => $quickers,
                ));
                if ( $the_query->have_posts() ){
        ?>
                    <div class="services-wrapper">
                        <div class="container">
                            <div class="row">
                                <?php
                                    while( $the_query->have_posts() ){
                                        $the_query->the_post();
                                ?>
                                        <div class="col-md-3">
                                            <div class="service-item">
                                                <?php if ( has_post_thumbnail()) { ?>
                                                <div class="service-image">
                                                    <?php the_post_thumbnail(); ?>
                                                </div>
                                                <?php } ?>
                                                <div class="service-content-wrapper">
                                                    <h3 class="service-title"><?php the_title(); ?></h3>
                                                    <div class="service-content">
                                                        <p><?php the_excerpt(); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                <?php }
                                    wp_reset_postdata(); ?>
                            </div>
                        </div>
                    </div>
            <?php }
            }
        ?>

        <?php
            $st_title = get_post_meta($postID, 'st_caption', true);
            if ( !empty($st_title) ){
                $st_link = get_post_meta($postID, 'st_link', true);
        ?>
                <div class="stunning-text-wraper">
                    <div class="container">
                        <div class="stunning-text">
                            <div class="stunning-text-item">
                                <h2 class="stunning-text-title"><?php echo $st_title ?></h2>
                                <p class="stunning-text-content"><?php echo get_post_meta($postID, 'st_content', true); ?></p>
                                <a href="<?php echo get_permalink($st_link) ?>" target="_blank" class="stunning-text-button">Learn more</a>
                            </div>
                        </div>
                    </div>
                </div>
     <?php } ?>

        <?php
        if ( is_active_sidebar('courses-sidebar') ){
            dynamic_sidebar('courses-sidebar');
        }
        ?>

        <?php
            $search_title = get_post_meta($postID, 'cl_search_title', true);
        if ( !empty($search_title) ){
            $search_caption = get_post_meta($postID, 'cl_search_caption', true);
            $bgID = get_post_meta($postID, 'cl_search_bg', true);
            $bg = wp_get_attachment_image_url($bgID, 'full');
            ?>
            <div class="search-wrapper" style="background-image: url('<?php echo $bg; ?>');  background-position: center 0px;">
                <div class="container">
                    <div class="search-title-wrapper">
                        <div class="search-title-head">
                            <h3 class="search-title"><?php echo $search_title; ?></h3>
                            <div class="title-devider"></div>
                            <div class="search-title-caption"><?php echo $search_caption; ?></div>
                        </div>
                        <div class="course-search-wrapper">
                            <form role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                                <div class="course-search-column-1">
                                    <select name="course_cat" id="">
                                        <option value="">Category</option>
                                        <?php
                                        $terms = get_terms(array('taxonomy' => 'cl_course_cat'));
                                        foreach ($terms as $term){
                                            echo '<option value="' . $term->slug . '">' . $term->name . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="course-search-column-2">
                                    <select name="course_type" id="">
                                        <option value="">Type</option>
                                        <?php
                                        $terms = get_terms(array('taxonomy' => 'cl_course_type'));
                                        foreach ($terms as $term){
                                            echo '<option value="' . $term->slug . '">' . $term->name . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="course-search-column-3">
                                    <input type="hidden" name="post_type" value="cl_course">
                                    <input id="s" name="s" autocomplete="off" type="text" placeholder="keywords" value="<?php echo get_search_query(); ?>">
                                </div>
                                <div class="course-search-column-4">
                                    <input type="submit" class="search-button" id="searchsubmit" value="Search!">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>

        <?php if ( is_active_sidebar('main-sidebar') ){ ?>
            <div class="page-content-wrapper">
                <div class="container">
            <?php dynamic_sidebar('main-sidebar'); ?>
                </div>
            </div>
        <?php } ?>

        <?php
        if ( is_active_sidebar('testimonial-sidebar') ){
            dynamic_sidebar('testimonial-sidebar');
        }
        ?>

        <?php
        if ( is_active_sidebar('banner-sidebar') ){
            dynamic_sidebar('banner-sidebar');
        }
        ?>

        <?php
        if ( is_active_sidebar('twitter-sidebar') ){
            dynamic_sidebar('twitter-sidebar');
        }
        ?>


    </div>

<?php get_footer(); ?>